#ifndef encoder_ctrl
#define encoder_ctrl

// Rotary encoder
#define SW 5
#define DT 4
#define CLK 3

inline int aState;
inline int aLastState;

inline int btn_already_pressed = 0;
inline int btn_set = 0;

void setup_encoder(void);
int check_button(void);
int get_encoder_state(void);

#endif
