#include <Arduino.h>
#include "charge_pin.h"


void setup_charge_pin()
{
    pinMode(BAT, INPUT);   
}

int check_charge_button()
{
    int result = 0;
  
    // Charge pin button
    if (!digitalRead(BAT)) {
        if (!charge_btn_already_pressed) {
            charge_btn_already_pressed = 1;

            result = 1;

            if (charge_btn_set) {
                charge_btn_set = 0;
            }
            else {
                charge_btn_set = 1;
            }
        }
    }
    else {
        charge_btn_already_pressed = 0;
    }

    return result;
}

int get_bat_charge()
{
    // Bat charge  
    // > 4.00 v = 100%
    // > 3.75 v = 75%
    // > 3.60 v = 50%
    // > 3.40 v = 25%
    // ...      = 0%

    long val = analogRead(BAT);
    float voltage = val * 5.0 / 1024;
    
    // Serial.println();

    if (voltage > 4) {
        return 5;
    }
    else if (voltage > 3.75) {
        return 4;
    }
    else if (voltage > 3.60) {
        return 3;
    }
    else if (voltage > 3.40) {
        return 2;
    }
    else {
        return 1;
    }
}
