#include <Adafruit_NeoPixel.h>
#include "color_led.h"

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(count_led, RGBPIN, NEO_GRB + NEO_KHZ800);

void setup_color_led()
{
    pixels.begin();
    pixels.setPixelColor(0, pixels.Color(0, 0, 255 / color_led_deem));
    pixels.show();
}

void set_green()
{
    pixels.setPixelColor(0, pixels.Color(0, 255 / color_led_deem, 0));
    pixels.show(); 
}

void set_red()
{
    pixels.setPixelColor(0, pixels.Color(255 / color_led_deem, 0, 0));
    pixels.show();
}

void set_purple()
{
    pixels.setPixelColor(0, pixels.Color(128 / color_led_deem, 0, 128 / color_led_deem));
    pixels.show();
}

void set_flash()
{
    float color_multiplier = pow(
      (float(abs(int(millis()/20 % 100) - 50)) / 50 * 0.8 + 0.2),
      75
    ) / color_led_deem;
    
    pixels.setPixelColor(0, pixels.Color(100 * color_multiplier, 50 * color_multiplier, 0));
    pixels.show();
}
