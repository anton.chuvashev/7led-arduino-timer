#include <Arduino.h>
#include "color_led.h"
#include "display_led.h"
#include "encoder_ctrl.h"
#include "charge_pin.h"
#include "player_box.h"

// Min/Sec
#define MS 10

unsigned long multiplier;

unsigned long current_time;
unsigned long stop_time;
unsigned long max_delta_time;
unsigned long pause_start_time;
unsigned long pause_full_time = 0UL;

int sound_enabled = 1;

int ms_btn_value = -1;


void setup()
{
    pinMode(MS, INPUT);

    setup_charge_pin();   
    setup_encoder();   
    setup_led();

    update_ms_btn_value();

    if (ms_btn_value) {
        charge_btn_set = 1;
    }

    setup_player();

    setup_color_led();

    disp_value = get_bat_charge();
    delay(1000);

    pause_start_time = millis() - 1000UL;
}

unsigned long get_current_time()
{
    int charge_btn_pressed = check_charge_button();

    unsigned long current_time = millis() - pause_full_time;

    if (charge_btn_pressed == 1) {
        if(disp_value == 0) {
          sound_enabled = 0;
        }

        if (charge_btn_set) {
            pause_init();
        }
        else {
            current_time = pause_release();
        }
    }

    if (charge_btn_set) {
        return pause_start_time;
    }
    
    return current_time;
}

void pause_init()
{
    charge_btn_set = 1;
    pause_start_time = current_time;
}

unsigned long pause_release()
{
    charge_btn_set = 0;

    unsigned long current_time = millis() - pause_full_time;

    pause_full_time += current_time - pause_start_time;

    current_time = millis() - pause_full_time;

    return current_time;
}

void update_ms_btn_value()
{
    if (ms_btn_value != digitalRead(MS)) {
        ms_btn_value = digitalRead(MS);

        if (ms_btn_value == 1) {
            pause_init();
        }
        else {
            pause_release();
        }

        set_time_params();
    }
}

void set_time_params()
{
    if(ms_btn_value == 1) {
        stop_time = get_current_time() + 30UL * 1000UL; // 30min
    
        max_delta_time = (60 * 4 + 59); // 299sec  
    }
    else {
        stop_time = get_current_time() + 30UL * 60UL * 1000UL; // 30min
    
        max_delta_time = (60 * 4 + 59) * 60; // 17940sec  
    }
}

void set_multiplier()
{
    if (btn_set) {
        if(ms_btn_value == 1) {
            multiplier = 10UL * 1000UL / 2UL;
        }
        else {
            multiplier = 600UL * 1000UL / 2UL;
        }
    }
    else {
        if(ms_btn_value == 1) {
            multiplier = 1000UL / 2UL;
        }
        else {
            multiplier = 60UL * 1000UL / 2UL;
        }
    }
}

void set_display_value(unsigned long time_left)
{ 
    if(ms_btn_value == 1) {
        disp_value = ceil(time_left / 1000.0);
    }
    else {
        disp_value = ceil(time_left / 60.0 / 1000);
    }
}

void loop()
{   
    update_ms_btn_value();

    current_time = get_current_time();
    
    int btn_pressed = check_button();

    if (btn_pressed == 1 && disp_value == 0) {
        sound_enabled = 0;
    }

    set_multiplier();

    int encoder_state = get_encoder_state();

    if (encoder_state != 0) {
        next_refresh_time = current_time + 100;
        sound_enabled = 1;

        if (encoder_state > 0 || multiplier < stop_time) {
          stop_time += multiplier * encoder_state;
        }
        else {
          stop_time = current_time;
        }
    }

    if (stop_time < current_time) {
        stop_time = current_time;
    }

    unsigned long time_left = stop_time - current_time;

    if (max_delta_time * 1000UL < time_left) {
        stop_time = current_time + max_delta_time * 1000UL;
    }

    set_display_value(time_left);

    if (time_left > 0) {
      stop_player();

      if (charge_btn_set) {
          set_purple();
      }
      else if (sound_enabled) {
          set_flash();
      }
      else {
          set_green();
      }
    }
    else {
      stop_time = current_time;
      
      if (sound_enabled) {
          set_red();

          start_player();
      }
      else {
          set_green();
                
          stop_player();
      }
    }
}
