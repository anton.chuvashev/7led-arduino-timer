#ifndef player_box
#define player_box

inline unsigned long player_start_time = 0;

void setup_player(void);
void start_player(void);
void stop_player(void);

#endif
