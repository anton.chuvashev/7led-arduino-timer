#ifndef charge_pin
#define charge_pin

// U-bat
#define BAT 9

void setup_charge_pin(void);
int get_bat_charge(void);
int check_charge_button(void);

inline int charge_btn_already_pressed = 0;
inline int charge_btn_set = 0;

#endif
