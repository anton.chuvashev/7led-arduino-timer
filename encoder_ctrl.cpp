#include <Arduino.h>
#include "encoder_ctrl.h"


void setup_encoder()
{
    pinMode(SW, INPUT);
    pinMode(DT, INPUT);
    pinMode(CLK, INPUT);

    aLastState = digitalRead(CLK); // Reads "start" state of the outputA
}

int check_button()
{
    int result = 0;
  
    // Кнопка енкодера
    if (digitalRead(SW)) {
        if (!btn_already_pressed) {
            btn_already_pressed = 1;

            result = 1;

            if (btn_set) {
                btn_set = 0;
            }
            else {
                btn_set = 1;
            }
        }
    }
    else {
        btn_already_pressed = 0;
    }

    return result;
}

int get_encoder_state()
{

    aState = digitalRead(CLK); // Reads "current" state of the outputA

    int result = 0;

    if (aState != aLastState) {
        if (digitalRead(DT) != aState) {
            result = 1;
        }
        else {
            result = -1;
        }
    }

    aLastState = aState;

    return result;
}
