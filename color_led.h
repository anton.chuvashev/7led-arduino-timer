#ifndef color_led
#define color_led

#define RGBPIN 2
#define count_led 1
#define color_led_deem 4

void setup_color_led(void);
void set_green(void);
void set_red(void);
void set_flash(void);
void set_purple(void);

#endif
