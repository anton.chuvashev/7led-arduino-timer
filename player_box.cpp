#include <Arduino.h>
#include "player_box.h"
#include "DYPlayerArduino.h"


DY::Player player(&Serial1);

void setup_player()
{
    player.begin();
}

void stop_player()
{
    player.stop();
}

void start_player()
{
    unsigned long played_seconds = (millis() - player_start_time) / 1000;

    if (played_seconds > 2 && player.checkPlayState() != DY::PlayState::Playing) {
        char path[] = "/00001.mp3";
        player.setVolume(20); 
        player.playSpecifiedDevicePath(DY::Device::Flash, path);
        player_start_time = millis();
    }
}
