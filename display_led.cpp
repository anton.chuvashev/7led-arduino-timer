#include <Arduino.h>
#include "display_led.h"


ISR(TIMER1_COMPA_vect)
{
    disp_off();

    if (next_refresh_time > millis()) {
        return;
    }

    switch (current_digit) {
        case 1:
            disp_led_row((disp_value / 60) % 10); // prepare to display digit 1
            digitalWrite(Dig1, LOW);              // turn on digit 1
        break;

        case 2:
            disp((disp_value / 10) % 6); // prepare to display digit 2
            digitalWrite(Dig2, LOW);      // turn on digit 2
        break;

        case 3:
            disp(disp_value % 10);   // prepare to display digit 3 (most right)
            digitalWrite(Dig3, LOW); // turn on digit 3
    }

    current_digit = (current_digit % 3) + 1;
}

void setup_led()
{
    set_led_pins();
    set_led_timer();
}

void set_led_timer()
{
    cli();      // отключить глобальные прерывания
    TCCR1A = 0; // установить TCCR1A регистр в 0
    TCCR1B = 0;

    OCR1A = 8192;            // установка регистра совпадения 30 * 4 ~ 120Hz
    TCCR1B |= (1 << WGM12);  // включение в CTC режим
    TCCR1B |= (1 << CS11);   // делитель /8: 16mHz / 65535 / 8 = 30,518 Hz (до переполнения)
    TIMSK1 |= (1 << OCIE1A); // включение прерываний по совпадению

    sei(); // включить глобальные прерывания
}  

void set_led_pins()
{
    pinMode(SegA, OUTPUT);
    pinMode(SegB, OUTPUT);
    pinMode(SegC, OUTPUT);
    pinMode(SegD, OUTPUT);
    pinMode(SegE, OUTPUT);
    pinMode(SegF, OUTPUT);
    pinMode(SegG, OUTPUT);

    pinMode(Dig1, OUTPUT);
    pinMode(Dig2, OUTPUT);
    pinMode(Dig3, OUTPUT);
}

void disp(byte number)
{
    disp_off();

    switch (number) {
        case 0:
            digitalWrite(SegA, LOW);
            digitalWrite(SegB, LOW);
            digitalWrite(SegC, LOW);
            digitalWrite(SegD, LOW);
            digitalWrite(SegE, LOW);
            digitalWrite(SegF, LOW);
            digitalWrite(SegG, HIGH);
        break;

        case 1:
            digitalWrite(SegA, HIGH);
            digitalWrite(SegB, LOW);
            digitalWrite(SegC, LOW);
            digitalWrite(SegD, HIGH);
            digitalWrite(SegE, HIGH);
            digitalWrite(SegF, HIGH);
            digitalWrite(SegG, HIGH);
        break;

        case 2:
            digitalWrite(SegA, LOW);
            digitalWrite(SegB, LOW);
            digitalWrite(SegC, HIGH);
            digitalWrite(SegD, LOW);
            digitalWrite(SegE, LOW);
            digitalWrite(SegF, HIGH);
            digitalWrite(SegG, LOW);
        break;

        case 3:
            digitalWrite(SegA, LOW);
            digitalWrite(SegB, LOW);
            digitalWrite(SegC, LOW);
            digitalWrite(SegD, LOW);
            digitalWrite(SegE, HIGH);
            digitalWrite(SegF, HIGH);
            digitalWrite(SegG, LOW);
        break;

        case 4:
            digitalWrite(SegA, HIGH);
            digitalWrite(SegB, LOW);
            digitalWrite(SegC, LOW);
            digitalWrite(SegD, HIGH);
            digitalWrite(SegE, HIGH);
            digitalWrite(SegF, LOW);
            digitalWrite(SegG, LOW);
        break;

        case 5:
            digitalWrite(SegA, LOW);
            digitalWrite(SegB, HIGH);
            digitalWrite(SegC, LOW);
            digitalWrite(SegD, LOW);
            digitalWrite(SegE, HIGH);
            digitalWrite(SegF, LOW);
            digitalWrite(SegG, LOW);
        break;

        case 6:
            digitalWrite(SegA, LOW);
            digitalWrite(SegB, HIGH);
            digitalWrite(SegC, LOW);
            digitalWrite(SegD, LOW);
            digitalWrite(SegE, LOW);
            digitalWrite(SegF, LOW);
            digitalWrite(SegG, LOW);
        break;

        case 7:
            digitalWrite(SegA, LOW);
            digitalWrite(SegB, LOW);
            digitalWrite(SegC, LOW);
            digitalWrite(SegD, HIGH);
            digitalWrite(SegE, HIGH);
            digitalWrite(SegF, HIGH);
            digitalWrite(SegG, HIGH);
        break;

        case 8:
            digitalWrite(SegA, LOW);
            digitalWrite(SegB, LOW);
            digitalWrite(SegC, LOW);
            digitalWrite(SegD, LOW);
            digitalWrite(SegE, LOW);
            digitalWrite(SegF, LOW);
            digitalWrite(SegG, LOW);
        break;

        case 9:
            digitalWrite(SegA, LOW);
            digitalWrite(SegB, LOW);
            digitalWrite(SegC, LOW);
            digitalWrite(SegD, LOW);
            digitalWrite(SegE, HIGH);
            digitalWrite(SegF, LOW);
            digitalWrite(SegG, LOW);
    }
}

void disp_led_row(byte number)
{
    switch (number) {
        case 0:
            digitalWrite(SegE, HIGH);
            digitalWrite(SegG, HIGH);
            digitalWrite(SegF, HIGH);
            digitalWrite(SegA, HIGH);
        break;

        case 1:
            digitalWrite(SegE, LOW);
            digitalWrite(SegG, HIGH);
            digitalWrite(SegF, HIGH);
            digitalWrite(SegA, HIGH);
        break;

        case 2:
            digitalWrite(SegE, LOW);
            digitalWrite(SegG, LOW);
            digitalWrite(SegF, HIGH);
            digitalWrite(SegA, HIGH);
        break;

        case 3:
            digitalWrite(SegE, LOW);
            digitalWrite(SegG, LOW);
            digitalWrite(SegF, LOW);
            digitalWrite(SegA, HIGH);
        break;

        case 4:
            digitalWrite(SegE, LOW);
            digitalWrite(SegG, LOW);
            digitalWrite(SegF, LOW);
            digitalWrite(SegA, LOW);
        break;
    }
}

void disp_off()
{
    digitalWrite(Dig1, HIGH);
    digitalWrite(Dig2, HIGH);
    digitalWrite(Dig3, HIGH);

    digitalWrite(SegA, HIGH);
    digitalWrite(SegB, HIGH);
    digitalWrite(SegC, HIGH);
    digitalWrite(SegD, HIGH);
    digitalWrite(SegE, HIGH);
    digitalWrite(SegF, HIGH);
    digitalWrite(SegG, HIGH);
}
