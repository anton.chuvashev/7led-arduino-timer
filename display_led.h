#ifndef display_led
#define display_led

// 7-segment led pin definitions
#define SegA 20 // Top + 4th led
#define SegB 21 // Top-Right
#define SegC 16 // Bottom-Right
#define SegD 14 // Bottom
#define SegE 15 // Bottom-left + 1st leg
#define SegF 19 // Top-Left + 3rd led
#define SegG 18 // Center + 2nd led

// common pins of the four digits led definitions
#define Dig1 8 // LED
#define Dig2 6 // Десятки
#define Dig3 7 // Единицы


inline byte current_digit;
inline unsigned long disp_value;
inline unsigned long next_refresh_time = 0;


void set_led_timer(void);
void set_led_pins(void);
void disp(byte);
void disp_led_row(byte);
void disp_off(void);
void setup_led(void);

#endif
